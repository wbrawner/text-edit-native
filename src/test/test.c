/*
 * =====================================================================================
 *
 *       Filename:  test.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  08/09/2019 17:05:45
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  William Brawner (Billy), billybrawner@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "minunit.h"
#include "../main/text_file.h"

int tests_run = 0;

TEXT_FILE *text_file = 0;
char *temp_file_template = "/tmp/text-XXXXXX";
char *temp_file_path = 0;

void test_setup() {
    temp_file_path = malloc(strlen(temp_file_template));
    strcpy(temp_file_path, temp_file_template);
    text_file = text_create_file(temp_file_path);
}

void test_cleanup() {
    text_clean(text_file);
    if (temp_file_path) {
        remove(temp_file_path);
        free(temp_file_path);
    }
}

static char * test_create_text_file() {
    mu_assert("text file creation failed", text_file);
    return 0;
}

static char * test_load_text_file() {
    text_clean(text_file);
    FILE *file = fopen(temp_file_path, "w+");
    fputs("# Test file\0", file);
    fclose(file);
    text_file = text_create_file(temp_file_path);
    mu_assert("text file loading failed", strcmp(text_file->contents, "# Test file\0") == 0);
    return 0;
}

static char *test_get_file_path() {
    char * text_file_path = text_get_file_path(text_file);
    int position = strstr(text_file_path, "/tmp/text-") - text_file_path;
    mu_assert("text file name retrieval failed", position == 0);
    return 0;
}

static char *test_get_file_name() {
    char * text_file_name = text_get_file_name(text_file);
    int position = strstr(text_file_name, "text-") - text_file_name;
    mu_assert("text file name retrieval failed", position == 0);
    return 0;
}

static char *test_get_parent_file_path() {
    char * text_parent_path = text_get_parent_file_path(text_file);
    int equals = strcmp(text_parent_path, "/tmp");
    mu_assert("text file parent path retrieval failed", equals == 0);
    return 0;
}

static char *test_update_contents() {
    char * new_contents = "# Test";
    text_update_contents(text_file, new_contents);
    int equals = strcmp(text_file->contents, new_contents);
    mu_assert("text file parent path retrieval failed", equals == 0);
    return 0;
}

static char *test_save_contents() {
    char * new_contents = "# Test";
    text_update_contents(text_file, new_contents);
    text_save_contents(text_file);
    FILE *saved_file = fopen(temp_file_path, "r");
    fseek(saved_file, 0L, SEEK_END);
    long file_length = ftell(saved_file);
    rewind(saved_file);
    char *saved_file_contents = malloc(file_length);
    fread(saved_file_contents, 1, file_length, saved_file);
    fclose(saved_file);
    int equals = strcmp(saved_file_contents, new_contents);
    mu_assert("text file saving failed", equals == 0);
    return 0;
}

typedef char* (*test_function)(void);

static char * all_tests() {
    test_function tests[] = {
        test_create_text_file,
        test_load_text_file,
        test_update_contents,
        test_save_contents,
        test_get_file_path,
        test_get_file_name,
        test_get_parent_file_path
    };
    for (int i = 0; i < sizeof(tests) / sizeof(test_function); i++) {
        test_setup();
        mu_run_test(tests[i]);
        test_cleanup();
    }
    return 0;
}

int main(int argc, char **argv) {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    } else {
        printf("ALL TESTS PASSED\n");
    }
    printf("Tests run: %d\n", tests_run);
    return result != 0;
}

