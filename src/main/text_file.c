#include "text_file.h"
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#ifdef _WIN32
const char FILE_PATH_SEPARATOR = '\\';
#else
const char FILE_PATH_SEPARATOR = '/';
#endif

TEXT_FILE *text_create_file(char *file_path) {
    // This is currently operating under the assumption that the directories
    // leading up to the file all exist already. Compared to programs like
    // vim, this seems acceptable and standard behavior.
    TEXT_FILE * text_file = malloc(sizeof(TEXT_FILE));
    if (!text_file) {
        return NULL;
    }
    text_file->file_path = malloc(strlen(file_path));
    strcpy(text_file->file_path, file_path);
    FILE *file = fopen(text_file->file_path, "r");
    if (file) {
        fseek(file, 0L, SEEK_END);
        long file_length = ftell(file);
        rewind(file);
        text_file->contents = malloc(file_length);
        fread(text_file->contents, 1, file_length, file);
        fclose(file);
    }
    return text_file;
}

void text_clean(TEXT_FILE *text_file) {
    if (!text_file) {
        return;
    }
    //if (text_file->file_path) free(text_file->file_path);
    //if (text_file->contents) free(text_file->contents);
    free(text_file);
}

// TODO: Return a relevant error code instead of a boolean
bool text_rename_file(TEXT_FILE *text_file, char *new_path) {
    if (rename(text_file->file_path, new_path) != 0) {
        return false;
    }

    text_file->file_path = realloc(text_file->file_path, strlen(new_path));
    strcpy(text_file->file_path, new_path);
    return true;
}

char *text_get_file_path(TEXT_FILE *text_file) {
    char * file_path = malloc(strlen(text_file->file_path));
    strcpy(file_path, text_file->file_path);
    return file_path;
}

char * text_get_parent_file_path(TEXT_FILE * text_file) {
    char * last_file_separator = strrchr(text_file->file_path, FILE_PATH_SEPARATOR);
    if (!last_file_separator) {
        printf("Unable to determine parent file path for text file at %s\n", text_file->file_path);
        return NULL;
    }
    size_t path_length = last_file_separator - text_file->file_path;
    char * parent_file_path = malloc(path_length);
    strncpy(parent_file_path, text_file->file_path, path_length);
    parent_file_path[path_length] = '\0';
    return parent_file_path;
}

char *text_get_file_name(TEXT_FILE *text_file) {
    char * last_file_separator = strrchr(text_file->file_path, '/');
    if (!last_file_separator) {
        printf("Unable to determine file name for text file at %s\n", text_file->file_path);
        return NULL;
    }
    size_t last_file_separator_pos = last_file_separator - text_file->file_path + 1;
    size_t file_length = strlen(text_file->file_path + last_file_separator_pos);
    char * file_name = malloc(file_length + 1);
    strncpy(file_name, text_file->file_path + last_file_separator_pos, file_length);
    file_name[file_length] = '\0';
    return file_name;
}

void text_update_contents(TEXT_FILE *text_file, char * updated_contents) {
    if (!text_file->contents) {
        text_file->contents = malloc(sizeof(updated_contents));
    } else if (sizeof(text_file->contents) != sizeof(updated_contents)) {
        text_file->contents = realloc(text_file->contents, sizeof(updated_contents));
    }
    strcpy(text_file->contents, updated_contents);
}

long text_save_contents(TEXT_FILE *text_file) {
    long bytes_written = 0;
    FILE *file = fopen(text_file->file_path, "we+");
    fwrite(text_file->contents, strlen(text_file->contents), 1, file);
    fclose(file);
    return bytes_written;
}

