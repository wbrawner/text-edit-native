#include <stdio.h>
#include <stdbool.h>

// TODO: Define error codes to return helpful information

typedef struct {
    char * contents;
    char * file_path;
} TEXT_FILE;

/**
 * Create or load a new text file at the given path
 *
 * @param file_path the absolute path to the text file to be created
 * @return a pointer to a TEXT_FILE struct or NULL on error
 */
TEXT_FILE * text_create_file(char * file_path);

/**
 * Load a file from disk with the given path
 *
 * @param text_file
 * @return a pointer to a TEXT_FILE struct or NULL on error
 */
TEXT_FILE * text_load_file(char * file_path);

/**
 * Clean up the memory allocated for a given text file. Note that you need
 * to save BEFORE calling this method, otherwise your changes will be lost.
 * @param text_file
 */
void text_clean(TEXT_FILE * text_file);

/**
 * Rename or move the file
 */
bool text_rename_file(TEXT_FILE *text_file, char *new_path);

/**
 * Get the absolute path to the file. Note that you will need to free the returned pointer yourself.
 * @return the absolute path of the text file's location on disk
 */
char * text_get_file_path(TEXT_FILE * text_file);

/**
 * Get the absolute path to the file's containing directory. Note that you will need to free the
 * returned pointer yourself.
 * @return the absolute path of the text file's location on disk
 */
char * text_get_parent_file_path(TEXT_FILE * text_file);

/**
 * Get the name of the file, without the path. Note that you will need to free the returned pointer
 * yourself.
 * @return the name of the text file
 */
char * text_get_file_name(TEXT_FILE * text_file);

void text_update_contents(TEXT_FILE *text_file, char * updated_contents);

/**
 * Save the contents of the text file to disk
 *
 * @return the number of bytes written, or -1 on failure
 */
long text_save_contents(TEXT_FILE * text_file);
